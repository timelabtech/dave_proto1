from flask import Flask, jsonify, abort, make_response, request

from model import point

app = Flask(__name__, static_url_path='', static_folder='../js/public')

# Static URL server JavaScript
app.add_url_rule('/', 'root', lambda: app.send_static_file('index.html'))

# Some REST API helper stuff
url_prefix = '/dave/api/v1.0/'

# Initial state
points = []
fit = {
    'period': 5.0,
    'amplitude': 10.0,
    'offset_x': 100.0,
    'offset_y': 0.0,
    }

def points2json():
    return jsonify({'points': list(map(point.Point.serialize, points))})

#
# REST API from here
#

# Get all points
@app.route(url_prefix + 'points', methods=['GET'])
def get_points():
    return points2json()

# Add one point
@app.route(url_prefix + 'points', methods=['POST'])
def add_point():
    points.append(point.gen_point())
    return points2json()

# Add one point
@app.route(url_prefix + 'points', methods=['DELETE'])
def delete_points():
    # Clear the list:
    del points[:]
    #print "Delete points. Now I have this many points", len(points)
    return points2json()

# Get the fit
@app.route(url_prefix + 'fit', methods=['GET'])
def get_fit():
    return jsonify(fit)

# Create a new fit
@app.route(url_prefix + 'fit', methods=['POST'])
def make_fit():
    fit['offset_y'] = fit['offset_y'] + 1
    return jsonify(fit)

# An error handler
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(debug=True)
